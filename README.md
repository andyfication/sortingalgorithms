This is my repo for the Sorting Algorithms project.


I worked with Goldsmiths, University of London and Coursera to develop some online interactive learning tools. The Sorting Algorithms is among these.

The Sorting Algorithms is an interactive simulation made in HTML5,CSS3 and javaScript.

The plugin shows step by step how Bubble Sort and Insertion Sort algorithms behave.

