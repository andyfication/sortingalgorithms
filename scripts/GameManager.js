// Class GameManager, handles the player and current algorithm type 
class GameManager {
  constructor(current_algorithm) {
    this.current_algorithm = current_algorithm;
    this.current_algorithm_id = current_algorithm.id;
    this.randomArray = this.current_algorithm.randomArrayCopy;
    this.player = null;
    // jQuery to grab all UI
    if (this.current_algorithm_id == "bubble") {
      this.listOfElements = $('.bubble-array-element');
      this.listOfCustomElements = $('[name = bubble_custom_array_element]');
      this.listOfSolutionElements = $('.bubble-array-element-solution');
      this.current_array_info = $('#bubble-current-array');
      this.sorted_array_info = $('#bubble-output-array');
      this.buttonSwap = $('#bubble-swap-elements');
      this.buttonSkipSwap = $('#bubble-next-elements');
      this.buttonSubmitAsnwer = $('#bubble-submit-answer');
      this.buttonResetGame = $('#bubble-reset-game');
      this.buttonResetGameCustom = $('#bubble-reset-game-custom');
      this.buttonDataType = $('#bubble-data-type');
      this.array_result = $('#bubble-array-result');
      this.array_swaps_result = $('#bubble-array-swaps-result');
      this.array_passages_result = $('#bubble-array-passages-result');
    } else if (this.current_algorithm_id == "insertion") {
      this.listOfElements = $('.insertion-array-element');
      this.listOfCustomElements = $('[name = insertion_custom_array_element]');
      this.listOfSolutionElements = $('.insertion-array-element-solution');
      this.current_array_info = $('#insertion-current-array');
      this.sorted_array_info = $('#insertion-output-array');
      this.buttonSwap = $('#insertion-swap-elements');
      this.buttonSkipSwap = $('#insertion-next-elements');
      this.buttonSubmitAsnwer = $('#insertion-submit-answer');
      this.buttonResetGame = $('#insertion-reset-game');
      this.buttonResetGameCustom = $('#insertion-reset-game-custom');
      this.buttonDataType = $('#insertion-data-type');
      this.array_result = $('#insertion-array-result');
      this.array_swaps_result = $('#insertion-array-swaps-result');
      this.array_passages_result = $('#insertion-array-passages-result');
    }
  }
  // Initialise process to setup the player and envirnoment based on algorithm type 
  init() {
    if (this.current_algorithm_id === "bubble") {
      this.player = new Player(this.listOfElements, this.current_algorithm_id);
      this.assignArrayToList(this.randomArray, this.listOfElements);
      this.assignArrayToCustomList(this.randomArray, this.listOfCustomElements);
      this.assignArrayToSolutionList(this.randomArray, this.listOfSolutionElements);
      this.displayCurrentArray(this.randomArray, this.current_array_info);
      this.displaySortedArray(this.current_algorithm.solution, this.sorted_array_info);
      this.player.initList();

      this.buttonSwap[0].addEventListener('click', () => {
        this.player.swapElements();
        this.player.updateListVisibility();
        console.log(this.player.swaps);
      });
      this.buttonSkipSwap[0].addEventListener('click', () => {
        this.player.skipElements();
        this.player.updateListVisibility();
        console.log(this.player.passes);
      });
      this.buttonSubmitAsnwer[0].addEventListener('click', () => {
        this.displayResults(this.player, this.current_algorithm, this.player.listToSort, this.current_algorithm.solution, this.array_result[0], this.array_swaps_result[0], this.array_passages_result[0]);
        $('#bubble-solution').css('display', 'block');
      });
      this.buttonResetGame[0].addEventListener('click', () => {
        this.resetRandom();
        $('#bubble-solution').css('display', 'none');
        $('#bubble-restart-container').toggle();
        $('#bubble-header-grid').css('pointer-events', 'all');
        $('#bubble-buttons-container-grid').css('pointer-events', 'all');
      });
      this.buttonResetGameCustom[0].addEventListener('click', () => {
        if (bubbleValidCustomArray) {
          this.resetCustom(this.listOfCustomElements, this.current_algorithm_id);
          $('#bubble-solution').css('display', 'none');
          $('#bubble-restart-container').toggle();
          $('#bubble-header-grid').css('pointer-events', 'all');
          $('#bubble-buttons-container-grid').css('pointer-events', 'all');
        }
      });
      this.buttonDataType[0].addEventListener('click', () => {
        $('#bubble-solution').css('display', 'none');
        bubbleUsingNumbers = !bubbleUsingNumbers;
        this.resetRandom();
        if (bubbleUsingNumbers) {
          this.buttonDataType.html("Strings");
          $('#bubble-restart-custom-info').html("Please insert integers between 1 and 99");
        } else {
          this.buttonDataType.html("Integers");
          $('#bubble-restart-custom-info').html("Please insert valid lower case alphabet values");
        }
      });
    } else if (this.current_algorithm_id === "insertion") {
      this.player = new Player(this.listOfElements, this.current_algorithm_id);
      this.assignArrayToList(this.randomArray, this.listOfElements);
      this.assignArrayToCustomList(this.randomArray, this.listOfCustomElements);
      this.assignArrayToSolutionList(this.randomArray, this.listOfSolutionElements);
      this.displayCurrentArray(this.randomArray, this.current_array_info);
      this.displaySortedArray(this.current_algorithm.solution, this.sorted_array_info);
      this.player.initList(); // Initialisation is done 

      this.buttonSwap[0].addEventListener('click', () => {
        this.player.swapElements();
        this.player.updateListVisibility();
      });

      this.buttonSkipSwap[0].addEventListener('click', () => {
        this.player.skipElements();
        this.player.updateListVisibility();
      });

      this.buttonSubmitAsnwer[0].addEventListener('click', () => {
        this.displayResults(this.player, this.current_algorithm, this.player.listToSort, this.current_algorithm.solution, this.array_result[0], this.array_swaps_result[0], this.array_passages_result[0]);
        $('#insertion-solution').css('display', 'block');
      });

      this.buttonResetGame[0].addEventListener('click', () => {
        this.resetRandom();
        $('#insertion-solution').css('display', 'none');
        $('#insertion-restart-container').toggle();
        $('#insertion-header-grid').css('pointer-events', 'all');
        $('#insertion-buttons-container-grid').css('pointer-events', 'all');
      });
      this.buttonResetGameCustom[0].addEventListener('click', () => {
        if (insertionValidCustomArray) {
          this.resetCustom(this.listOfCustomElements, this.current_algorithm_id);
          $('#insertion-solution').css('display', 'none');
          $('#insertion-restart-container').toggle();
          $('#insertion-header-grid').css('pointer-events', 'all');
          $('#insertion-buttons-container-grid').css('pointer-events', 'all');
        }
      });
      this.buttonDataType[0].addEventListener('click', () => {
        $('#insertion-solution').css('display', 'none');
        insertionUsingNumbers = !insertionUsingNumbers;
        this.resetRandom();
        if (insertionUsingNumbers) {
          this.buttonDataType.html("Strings");
          $('#insertion-restart-custom-info').html("Please insert integers between 1 and 99");
        } else {
          this.buttonDataType.html("Integers");
          $('#insertion-restart-custom-info').html("Please insert valid lower case alphabet values");
        }
      });
    }
  }
  // Reset the player and envirnoment variables when using a random array 
  resetRandom() {
    this.current_algorithm.resetRandom();
    this.randomArray = this.current_algorithm.randomArrayCopy;
    this.assignArrayToList(this.randomArray, this.listOfElements);
    this.assignArrayToCustomList(this.randomArray, this.listOfCustomElements);
    this.assignArrayToSolutionList(this.randomArray, this.listOfSolutionElements);
    this.displayCurrentArray(this.randomArray, this.current_array_info);
    this.displaySortedArray(this.current_algorithm.solution, this.sorted_array_info);
    this.player.initList();
    this.player.updateListVisibility();
    this.array_result[0].innerHTML = "No solution provided yet</br> I guess you are still sorting";
    this.array_swaps_result[0].innerHTML = "";
    this.array_passages_result[0].innerHTML = "";
  }

  resetCustom(list, algorithm_id) {
    if (algorithm_id == "bubble") {
      if (bubbleUsingNumbers) {
        this.current_algorithm.resetCustomNumber(list);
      } else if (!bubbleUsingNumbers || !insertionUsingNumbers) {
        this.current_algorithm.resetCustomString(list);
      }
    } else if (algorithm_id == "insertion") {
      if (insertionUsingNumbers) {
        this.current_algorithm.resetCustomNumber(list);
      } else if (!bubbleUsingNumbers || !insertionUsingNumbers) {
        this.current_algorithm.resetCustomString(list);
      }
    }
    this.randomArray = this.current_algorithm.randomArrayCopy;
    this.assignArrayToList(this.randomArray, this.listOfElements);
    this.assignArrayToCustomList(this.randomArray, this.listOfCustomElements);
    this.assignArrayToSolutionList(this.randomArray, this.listOfSolutionElements);
    this.displayCurrentArray(this.randomArray, this.current_array_info);
    this.displaySortedArray(this.current_algorithm.solution, this.sorted_array_info);
    this.player.initList();
    this.player.updateListVisibility();
    this.array_result[0].innerHTML = "No solution provided yet</br> I guess you are still sorting";
    this.array_swaps_result[0].innerHTML = "";
    this.array_passages_result[0].innerHTML = "";
  }
  // Fill the unordered list with random array values 
  assignArrayToList(array, list) {
    if (this.current_algorithm_id == "bubble") {
      for (var index = 0; index < array.length; index++) {
        list[index].innerHTML = '<span class="bubble-element">' + array[index] + '</span>';
      }
    } else if (this.current_algorithm_id == "insertion") {
      for (var index = 0; index < array.length; index++) {
        list[index].innerHTML = '<span class="insertion-element">' + array[index] + '</span>';
      }
    }
  }

  // Fill the custom array with the current list
  assignArrayToCustomList(array, list) {
    if (this.current_algorithm_id == "bubble") {
      for (var index = 0; index < array.length; index++) {
        list[index].value = array[index];
      }
    } else if (this.current_algorithm_id == "insertion") {
      for (var index = 0; index < array.length; index++) {
        list[index].value = array[index];
      }
    }
  }

  // Fill the solution list with current list 
  assignArrayToSolutionList(array, list) {
    if (this.current_algorithm_id == "bubble") {
      for (var index = 0; index < array.length; index++) {
        list[index].innerHTML = '<span class="bubble-element-solution">' + array[index] + '</span>';
      }
    } else if (this.current_algorithm_id == "insertion") {
      for (var index = 0; index < array.length; index++) {
        list[index].innerHTML = '<span class="insertion-element-solution">' + array[index] + '</span>';
      }
    }
  }


  // Display information about the algorithm you are sorting 
  displayCurrentArray(array, element) {
    let tempString = "";
    tempString += "You are sorting : <span class = 'sorting_array_info'> ";
    array.forEach(item => {
      tempString += ' [' + item + ']';
    });
    tempString += "</span>";
    element[0].innerHTML = tempString;

  }

  // Display information about the algorithm solution 
  displaySortedArray(array, element) {
    let tempString = "";
    tempString += "The sorted array is : <span class = 'sorted_array_info'> ";
    array.forEach(item => {
      tempString += ' [' + item + ']';
    });
    tempString += "</span>";
    element[0].innerHTML = tempString;
  }

  // Display player vs algorithm solution ( sorted ? ,number of passes and number of swaps )
  displayResults(player_obj, bubbleSrt_obj, playerList, solutionArray, arr_res, arr_swp, arr_pass) {

    let playerArray = [];
    if (this.current_algorithm_id == "bubble") {
      if (bubbleUsingNumbers) {
        for (var index = 0; index < playerList.length; index++) {
          playerArray.push(parseInt($(playerList[index]).text()));
        }
      } else {
        for (var index = 0; index < playerList.length; index++) {
          playerArray.push($(playerList[index]).text());
        }
      }
    } else if (this.current_algorithm_id == "insertion") {
      if (insertionUsingNumbers) {
        for (var index = 0; index < playerList.length; index++) {
          playerArray.push(parseInt($(playerList[index]).text()));
        }
      } else {
        for (var index = 0; index < playerList.length; index++) {
          playerArray.push($(playerList[index]).text());
        }
      }
    }
    let result = this.arraysEqual(playerArray, solutionArray);

    if (this.current_algorithm_id == "bubble") {
      if (result) {
        arr_res.innerHTML = "The array is SORTED";
        arr_swp.innerHTML = "Player Swaps: " + player_obj.swaps + " / Algorithm Swaps: " + bubbleSrt_obj.swaps + '<hr class = rs_line>';
        arr_pass.innerHTML = "Player Passes: " + player_obj.passes + " / Algorithm Passes: " + bubbleSrt_obj.passages + '<hr class = rs_line>';
      } else {
        arr_res.innerHTML = "The array is NOT SORTED";
        arr_swp.innerHTML = "Player Swaps: " + player_obj.swaps + " / Algorithm Swaps: " + bubbleSrt_obj.swaps + '<hr class = rs_line>';
        arr_pass.innerHTML = "Player Passes: " + player_obj.passes + " / Algorithm Passes: " + bubbleSrt_obj.passages + '<hr class = rs_line>';
      }
    } else if (this.current_algorithm_id == "insertion") {
      if (result) {
        arr_res.innerHTML = "The array is SORTED";
        arr_swp.innerHTML = "Player Swaps: " + player_obj.swaps + " / Algorithm Swaps: " + bubbleSrt_obj.swaps + '<hr class = rs_line>';
        arr_pass.innerHTML = "Player Passes: 1 " + "/ Algorithm Passes: 1 " + '<hr class = rs_line>';
      } else {
        arr_res.innerHTML = "The array is NOT SORTED";
        arr_swp.innerHTML = "Player Swaps: " + player_obj.swaps + " / Algorithm Swaps: " + bubbleSrt_obj.swaps + '<hr class = rs_line>';
        arr_pass.innerHTML = "Player Passes: 1 " + "/ Algorithm Passes: 1 " + '<hr class = rs_line>';
      }
    }
  }
  // function to check if two arrays contains the same elements
  arraysEqual(arr1, arr2) {
    if (arr1.length !== arr2.length)
      return false;
    for (var i = arr1.length; i--;) {
      if (arr1[i] !== arr2[i])
        return false;
    }
    return true;
  }
}