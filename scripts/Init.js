// Main javascript function. The gameManager handles the player moves and current algorithm.
$(document).ready(() => {
  init_app();
});

// this will be used for the coursera API
courseraApi.callMethod({
  type: "GET_SESSION_CONFIGURATION",
  onSuccess: init_app
});

// Initialise app 
function init_app() {
  let bubble = new BubbleSort();
  let insertion = new InsertionSort();
  let gameManagerBubble = new GameManager(bubble);
  let gameManagerInsertion = new GameManager(insertion);
  gameManagerBubble.init();
  gameManagerInsertion.init();
  checkAlgorithmToDisplay();

}
// checks which algorithm container to show 
function checkAlgorithmToDisplay() {
  $('input[name="algorithm"]').on('change', function () {
    if ($(this).val() == 'bubble') {
      $("#insertion-container-grid").fadeOut('slow', () => {
        $("#bubble-bubble-check").prop("checked", true);
        $("#bubble-container-grid").fadeIn('slow');
        $("#bubble-container-grid").css('display', 'grid');
      });
    } else if ($(this).val() == 'insertion') {
      $("#bubble-container-grid").fadeOut('slow', () => {
        $("#insertion-insertion-check").prop("checked", true);
        $("#insertion-container-grid").fadeIn('slow');
        $("#insertion-container-grid").css('display', 'grid');
      });
    }
  });
}