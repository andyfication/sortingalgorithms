// Used to check for ui validation when generating a custom array
let bubbleValidCustomArray = true;
let insertionValidCustomArray = true;

// Used to check the data type of our sorting in both Bubble and Insertion sort
let bubbleUsingNumbers = true;
let insertionUsingNumbers = true;

$(document).ready(function () {
  let main_container = $('#main-container-grid');
  let bubble_container = $('#bubble-container-grid');
  let insertion_container = $('#insertion-container-grid');
  // grab the custom lists on game restart 
  let bubbleCustomList = $('[name = bubble_custom_array_element]');
  let insertionCustomList = $('[name = insertion_custom_array_element]');
  // grab the feedback text on game restart
  let bubbleCustomText = $('#bubble-restart-custom-info');
  let insertionCustomText = $('#insertion-restart-custom-info');

  // Check when inserting a custom list 
  bubbleCustomList.change(function () {
    if (bubbleUsingNumbers) {
      let firstCondition = false;
      let secondCondition = false;
      let thirdCondition = false;
      for (let index = 0; index < bubbleCustomList.length; index++) {
        if (bubbleCustomList[index].value === "" || bubbleCustomList[index].value === " " || bubbleCustomList[index].value === "  ") {
          bubbleCustomText.html("Please fill in the whole array");
          bubbleValidCustomArray = false;
          firstCondition = false;
          break;
        } else {
          firstCondition = true;
        }
      }
      if (firstCondition) {
        for (let index = 0; index < bubbleCustomList.length; index++) {
          if (isNaN(bubbleCustomList[index].value) || bubbleCustomList[index].value[0] == "." || bubbleCustomList[index].value[1] == ".") {
            bubbleCustomText.html("Please insert valid numbers");
            bubbleValidCustomArray = false;
            secondCondition = false;
            break;
          } else {
            secondCondition = true;
          }
        }
      }
      if (secondCondition) {
        for (let index = 0; index < bubbleCustomList.length; index++) {
          if (parseInt(bubbleCustomList[index].value) < 0 || parseInt(bubbleCustomList[index].value) >= 100) {
            bubbleCustomText.html("Please insert integers between 1 and 99");
            firstCondition = false;
            secondCondition = false;
            thirdCondition = false;
            bubbleValidCustomArray = false;
            break;
          } else {
            thirdCondition = true;
          }
        }
      }
      if (thirdCondition) {
        bubbleCustomText.html("You can use this array");
        firstCondition = false;
        secondCondition = false;
        thirdCondition = false;
        bubbleValidCustomArray = true;
      }
    } else {
      let firstCondition = false;
      let secondCondition = false;
      for (let index = 0; index < bubbleCustomList.length; index++) {
        if (bubbleCustomList[index].value === "" || bubbleCustomList[index].value === " " || bubbleCustomList[index].value === "  ") {
          bubbleCustomText.html("Please fill in the whole array");
          bubbleValidCustomArray = false;
          firstCondition = false;
          break;
        } else {
          firstCondition = true;
        }
      }
      if (firstCondition) {
        var letters = /^[a-z]+$/;
        for (let index = 0; index < bubbleCustomList.length; index++) {
          if (!bubbleCustomList[index].value.match(letters) || bubbleCustomList[index].value[0] == " " || bubbleCustomList[index].value[1] == " ") {
            bubbleCustomText.html("Please insert valid lower case alphabet values");
            bubbleValidCustomArray = false;
            secondCondition = false;
            break;
          } else {
            secondCondition = true;
          }
        }
      }
      if (secondCondition) {
        bubbleCustomText.html("You can use this array");
        firstCondition = false;
        secondCondition = false;
        thirdCondition = false;
        bubbleValidCustomArray = true;
      }
    }
  });




  insertionCustomList.change(function () {
    if (insertionUsingNumbers) {
      let firstCondition = false;
      let secondCondition = false;
      let thirdCondition = false;
      for (let index = 0; index < insertionCustomList.length; index++) {
        if (insertionCustomList[index].value === "" || insertionCustomList[index].value === " " || insertionCustomList[index].value === "  ") {
          insertionCustomText.html("Please fill in the whole array");
          insertionValidCustomArray = false;
          firstCondition = false;
          break;
        } else {
          firstCondition = true;
        }
      }
      if (firstCondition) {
        for (let index = 0; index < insertionCustomList.length; index++) {
          if (isNaN(insertionCustomList[index].value) || insertionCustomList[index].value[0] == "." || insertionCustomList[index].value[1] == ".") {
            insertionCustomText.html("Please insert valid numbers");
            insertionValidCustomArray = false;
            secondCondition = false;
            break;
          } else {
            secondCondition = true;
          }
        }
      }
      if (secondCondition) {
        for (let index = 0; index < insertionCustomList.length; index++) {
          if (parseInt(insertionCustomList[index].value) < 0 || parseInt(insertionCustomList[index].value) >= 100) {
            insertionCustomText.html("Please insert integers between 1 and 99");
            firstCondition = false;
            secondCondition = false;
            thirdCondition = false;
            insertionValidCustomArray = false;
            break;
          } else {
            thirdCondition = true;
          }
        }
      }
      if (thirdCondition) {
        insertionCustomText.html("You can use this array");
        firstCondition = false;
        secondCondition = false;
        thirdCondition = false;
        insertionValidCustomArray = true;
      }
    } else {
      let firstCondition = false;
      let secondCondition = false;
      for (let index = 0; index < insertionCustomList.length; index++) {
        if (insertionCustomList[index].value === "" || insertionCustomList[index].value === " " || insertionCustomList[index].value === "  ") {
          insertionCustomText.html("Please fill in the whole array");
          insertionValidCustomArray = false;
          firstCondition = false;
          break;
        } else {
          firstCondition = true;
        }
      }
      if (firstCondition) {
        var letters = /^[a-z]+$/;
        for (let index = 0; index < insertionCustomList.length; index++) {
          if (!insertionCustomList[index].value.match(letters) || insertionCustomList[index].value[0] == " " || insertionCustomList[index].value[1] == " ") {
            insertionCustomText.html("Please insert valid lower case alphabet values");
            insertionValidCustomArray = false;
            secondCondition = false;
            break;
          } else {
            secondCondition = true;
          }
        }
      }
      if (secondCondition) {
        insertionCustomText.html("You can use this array");
        firstCondition = false;
        secondCondition = false;
        thirdCondition = false;
        insertionValidCustomArray = true;
      }
    }
  });

  $('#bubble-check').click(function () {
    insertion_container.hide();
    bubble_container.fadeIn('slow');
  });

  $('#insertion-check').click(function () {
    bubble_container.hide();
    insertion_container.fadeIn('slow');
  });

  $('#bubble-tutorial').click(function () {
    $(".down-arrow").css('display', 'block');
    $(".modal-title").html(modalBody[1].title);
    $(".modal-body").html(modalBody[1].body);
    $(".modal").modal("show");
  });

  $('#insertion-tutorial').click(function () {
    $(".down-arrow").css('display', 'block');
    $(".modal-title").html(modalBody[2].title);
    $(".modal-body").html(modalBody[2].body);
    $(".modal").modal("show");
  });

  $('#bubble-new-array').click(function () {
    $('#bubble-restart-container').toggle();
    $('#bubble-header-grid').css('pointer-events', 'none');
    $('#bubble-buttons-container-grid').css('pointer-events', 'none');
  });

  $('#insertion-new-array').click(function () {
    $('#insertion-restart-container').toggle();
    $('#insertion-header-grid').css('pointer-events', 'none');
    $('#insertion-buttons-container-grid').css('pointer-events', 'none');
  });

  $('#bubble-solution').click(function () {
    $('#bubble-solution-container').toggle();
    $('#bubble-header-grid').css('pointer-events', 'none');
    $('#bubble-buttons-container-grid').css('pointer-events', 'none');
  });

  $('#insertion-solution').click(function () {
    $('#insertion-solution-container').toggle();
    $('#insertion-header-grid').css('pointer-events', 'none');
    $('#insertion-buttons-container-grid').css('pointer-events', 'none');
  });

  $('#bubble-exit').click(function () {
    $('#bubble-restart-container').toggle();
    $('#bubble-header-grid').css('pointer-events', 'all');
    $('#bubble-buttons-container-grid').css('pointer-events', 'all');
  });

  $('#insertion-exit').click(function () {
    $('#insertion-restart-container').toggle();
    $('#insertion-header-grid').css('pointer-events', 'all');
    $('#insertion-buttons-container-grid').css('pointer-events', 'all');
  });

  $('#bubble-solution-exit').click(function () {
    $('#bubble-solution-container').toggle();
    $('#bubble-header-grid').css('pointer-events', 'all');
    $('#bubble-buttons-container-grid').css('pointer-events', 'all');
  });

  $('#insertion-solution-exit').click(function () {
    $('#insertion-solution-container').toggle();
    $('#insertion-header-grid').css('pointer-events', 'all');
    $('#insertion-buttons-container-grid').css('pointer-events', 'all');
  });



  let waitingForEndAnimation = setInterval(() => {
    if (mainAimationFinished) {
      main_container.css('display', 'block');
      bubble_container.fadeIn('slow');
      bubble_container.css('display', 'grid');
      $(".modal-title").html(modalBody[0].title);
      $(".modal-body").html(modalBody[0].body);
      $(".modal").modal("show");
      clearInterval(waitingForEndAnimation);
    }
  }, 1000);
});