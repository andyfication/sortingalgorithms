// Class player which for now handles the bubbleSort algorithm
class Player {
  constructor(listToPlayWith, algorithm_id) {
    this.algorithm_id = algorithm_id;
    // Bubble Sort constructor
    if (this.algorithm_id === "bubble") {
      this.listToSort = listToPlayWith;
      this.passes = 1;
      this.swaps = 0;
      this.prev_swaps = 0;
      this.currentListIndex = 0;
      this.listUnsortedElements = 5;
      this.shouldBeSorted = false;
      // Insertion Sort constructor
    } else if (this.algorithm_id === "insertion") {
      this.listToSort = listToPlayWith;
      this.currentListIndex = 1;
      this.sortedList = [];
      this.currentSortedIndex = 0;
      this.swaps = 0;
      this.reachedEndList = false;
      this.areWeSwapping = false;
      this.headSwap = true;
      this.shouldBeSorted = false;
    }
  }

  // Perform the swap
  swapElements() {
    if (this.algorithm_id === "bubble") {
      if (!this.shouldBeSorted) {
        let tempSwap = this.listToSort[this.currentListIndex].innerHTML;
        this.listToSort[this.currentListIndex].innerHTML = this.listToSort[this.currentListIndex + 1].innerHTML;
        this.listToSort[this.currentListIndex + 1].innerHTML = tempSwap;
        this.swaps += 1;
        this.nextListIndex();
      }
    } else if (this.algorithm_id === "insertion") {
      // check when swapping at the beginning of the array
      if (!this.shouldBeSorted) {
        if (this.sortedList.length == 6 && this.currentSortedIndex == 1) {
          let tempValue = this.listToSort[this.currentSortedIndex].innerHTML;
          this.listToSort[this.currentSortedIndex].innerHTML = this.listToSort[this.currentSortedIndex - 1].innerHTML;
          this.listToSort[this.currentSortedIndex - 1].innerHTML = tempValue;
          this.swaps += 1;
          this.shouldBeSorted = true;
        }
      }
      // If not solution has benn found and we have and we only have the first two items
      if (!this.shouldBeSorted) {
        if (this.currentListIndex <= 1) {
          let tempValue = this.listToSort[this.currentListIndex].innerHTML;
          this.listToSort[this.currentListIndex].innerHTML = this.listToSort[this.currentSortedIndex].innerHTML;
          this.listToSort[this.currentSortedIndex].innerHTML = tempValue;
          this.currentListIndex += 1;
          this.currentSortedIndex += 1;
          this.sortedList.push(this.listToSort[this.currentSortedIndex]);
          this.swaps += 1;
          // If not solution has benn found and we have more than two items
        } else if (this.currentListIndex > 1) {

          this.areWeSwapping = true;
          // Checking swap when we have only 3 elements 
          if (this.currentSortedIndex == 1 && this.sortedList.length >= 3) {
            let tempValue = this.listToSort[this.currentSortedIndex].innerHTML;
            this.listToSort[this.currentSortedIndex].innerHTML = this.listToSort[this.currentSortedIndex - 1].innerHTML;
            this.listToSort[this.currentSortedIndex - 1].innerHTML = tempValue;
            this.currentSortedIndex = 0;
            this.swaps += 1;
          }

          if (this.currentSortedIndex >= 1) {
            // Is this the first swap ? Add the current element to the sorted list 
            if (this.headSwap) {
              let tempValue = this.listToSort[this.currentListIndex].innerHTML;
              this.listToSort[this.currentListIndex].innerHTML = this.listToSort[this.currentSortedIndex].innerHTML;
              this.listToSort[this.currentSortedIndex].innerHTML = tempValue;
              this.currentListIndex += 1;
              this.currentSortedIndex += 1;
              this.sortedList.push(this.listToSort[this.currentSortedIndex]);
              this.currentSortedIndex -= 1;
              this.headSwap = false;
              this.swaps += 1;
              // check where to place the element inside the sorted list 
            } else {
              let tempValue = this.listToSort[this.currentSortedIndex].innerHTML;
              this.listToSort[this.currentSortedIndex].innerHTML = this.listToSort[this.currentSortedIndex - 1].innerHTML;
              this.listToSort[this.currentSortedIndex - 1].innerHTML = tempValue;
              this.currentSortedIndex -= 1;
              this.swaps += 1;
            }
            // end of list, done swapping. Reset head.
          } else {
            this.currentSortedIndex = this.sortedList.length - 1;
            this.areWeSwapping = false;
            this.headSwap = true;
          }
        }
      }
    }
  }
  // Increment array index position when no swap 
  skipElements() {
    if (this.algorithm_id === "bubble") {
      this.nextListIndex();
    } else if (this.algorithm_id === "insertion") {
      this.nextListIndex();
    }
  }

  // Increment array index function 
  nextListIndex() {
    if (this.algorithm_id === "bubble") {
      if (!this.shouldBeSorted) {
        this.currentListIndex = (this.currentListIndex += 1) % this.listUnsortedElements;
        if (this.currentListIndex === 0) {
          console.log(this.swaps + "swaps");

          if (this.swaps == this.prev_swaps) {
            for (var index = 0; index < this.listUnsortedElements + 1; index++) {
              $(this.listToSort[index]).removeClass('hide_element');
              $(this.listToSort[index]).addClass('sorted_element');
              this.shouldBeSorted = true;
            }
          } else if (this.listUnsortedElements > 1) {
            this.listUnsortedElements -= 1;
            $(this.listToSort[this.listUnsortedElements + 1]).removeClass('hide_element');
            $(this.listToSort[this.listUnsortedElements + 1]).addClass('sorted_element');
            this.shouldBeSorted = false;
          } else if (this.listUnsortedElements === 1) {
            $(this.listToSort[this.listUnsortedElements - 1]).removeClass('hide_element');
            $(this.listToSort[this.listUnsortedElements - 1]).addClass('sorted_element');
            $(this.listToSort[this.listUnsortedElements]).removeClass('hide_element');
            $(this.listToSort[this.listUnsortedElements]).addClass('sorted_element');
            this.shouldBeSorted = true;
          }
          if (!this.shouldBeSorted)
            this.passes += 1;
          this.prev_swaps = this.swaps;
        }
      }
    } else if (this.algorithm_id === "insertion") {
      if (!this.areWeSwapping) {
        // Adds last element of the unsorted list to the sorted list
        if (this.currentListIndex == this.listToSort.length - 1 && !this.reachedEndList) {
          this.reachedEndList = true;
          this.shouldBeSorted = true;
          this.currentSortedIndex += 1;
          this.sortedList.push(this.listToSort[this.currentSortedIndex]);
        }
        // Checks if we are at the end of the list. If not increases the counters and pushes elements to the sorted list 
        if (!this.reachedEndList) {
          this.currentListIndex = this.currentListIndex < this.listToSort.length - 1 ? this.currentListIndex += 1 : this.currentListIndex = this.listToSort.length - 1;
          this.currentSortedIndex = this.currentListIndex - 1;
          this.sortedList.push(this.listToSort[this.currentSortedIndex]);
        }
        // We are swapping now 
      } else {
        // beginning of list, the soultion should be found 
        if (this.sortedList.length == 6 && this.currentSortedIndex == 1) {
          this.shouldBeSorted = true;
        }
        // end of list, the soultion should be found 
        if (this.sortedList.length == 6) {
          this.shouldBeSorted = true;
        }
        // if we are not at the beginnin or end of the list, we are not longer swapping and we reset the current index 
        else {
          this.areWeSwapping = false;
          this.headSwap = true;
          this.currentSortedIndex = this.sortedList.length - 1;
        }
      }
    }
  }
  // Initialise player with curretn list to sort 
  initList() {
    if (this.algorithm_id === "bubble") {
      this.passes = 1;
      this.swaps = 0;
      this.prev_swaps = 0;
      this.currentListIndex = 0;
      this.listUnsortedElements = 5;
      this.shouldBeSorted = false;

      for (var index = 0; index < this.listToSort.length; index++) {
        if (index == 0 || index == 1) {
          $(this.listToSort[index]).removeClass('sorted_element');
        } else {
          $(this.listToSort[index]).addClass('hide_element');
          $(this.listToSort[index]).removeClass('sorted_element');
        }
      }
    } else if (this.algorithm_id === "insertion") {
      this.sortedList = [];
      this.swaps = 0;
      this.currentListIndex = 1;
      this.currentSortedIndex = 0;
      this.headSwap = true;
      this.shouldBeSorted = false;
      this.areWeSwapping = false;
      this.reachedEndList = false;
      $(this.listToSort[this.currentSortedIndex]).addClass('sorted_element');
      $(this.listToSort[this.currentSortedIndex]).addClass('highLight_element');
      $(this.listToSort[this.currentListIndex]).addClass('highLight_element');
      for (var index = 2; index < this.listToSort.length; index++) {
        $(this.listToSort[index]).addClass('hide_element');
      }
      this.sortedList.push(this.listToSort[0]);
    }
  }




  // Update list visibility, only shows two items at the time. 
  updateListVisibility() {
    if (this.algorithm_id === "bubble") {
      for (var index = 0; index < this.listUnsortedElements + 1; index++) {
        if (!this.shouldBeSorted)
          $(this.listToSort[index]).addClass('hide_element');
      }
      $(this.listToSort[this.currentListIndex]).removeClass('hide_element');
      $(this.listToSort[this.currentListIndex + 1]).removeClass('hide_element');

      // check for sorte elements at the end of the list

    } else if (this.algorithm_id === "insertion") {
      //-------------------------------Only when just do not swap actions are taken------------
      if (!this.areWeSwapping) {
        for (var index = 0; index < this.listToSort.length; index++) {
          $(this.listToSort[index]).removeClass('highLight_element');
          $(this.listToSort[index]).removeClass('sorted_element');
        }

        for (var index = 0; index < this.sortedList.length; index++) {
          $(this.listToSort[index]).addClass('sorted_element');
        }

        $(this.listToSort[this.currentListIndex]).addClass('highLight_element');
        $(this.listToSort[this.currentListIndex]).removeClass('hide_element');
        $(this.listToSort[this.currentListIndex]).removeClass('sorted_element');
        $(this.listToSort[this.currentSortedIndex]).addClass('highLight_element');
        //----------check the end of list -------------------------------
        if (this.sortedList.length === this.listToSort.length) {
          $(this.listToSort[this.currentListIndex]).removeClass('highLight_element');
          $(this.listToSort[this.currentListIndex]).addClass('sorted_element');
        }
      } // We are not longer swapping 
      else {
        for (var index = 0; index < this.listToSort.length; index++) {
          $(this.listToSort[index]).removeClass('highLight_element');
          $(this.listToSort[index]).removeClass('sorted_element');
        }
        for (var index = 0; index < this.sortedList.length; index++) {
          $(this.listToSort[index]).addClass('sorted_element');
        }
        $(this.listToSort[this.currentSortedIndex]).addClass('highLight_element');
        $(this.listToSort[this.currentSortedIndex - 1]).addClass('highLight_element');

        if (this.shouldBeSorted) {
          $(this.listToSort[this.currentSortedIndex]).removeClass('highLight_element');
          $(this.listToSort[this.currentSortedIndex - 1]).removeClass('highLight_element');
        }
      }
    }
  }
}