// class insertion sort which performs the algorithm check 
class InsertionSort {

  constructor() {
    this.id = "insertion";
    this.randomArray = this.generateRandomArray(6);
    this.randomArrayCopy = this.getCopyOfgeneratedArray();
    this.swaps = 0;
    this.solution = [];
    this.solutionText = $('#insertion-solution-body');
    this.solutionText[0].innerHTML = '<p class = "pass-text"> Pass Number ' + 1 + '</p>';
    this.getSolution();
  }
  // reset the algorithm with a random array
  resetRandom() {
    this.randomArray = this.generateRandomArray(6);
    this.randomArrayCopy = this.getCopyOfgeneratedArray();
    this.swaps = 0;
    this.solutionText[0].innerHTML = '<p class = "pass-text"> Pass Number ' + 1 + '</p>';
    this.getSolution();
  }
  // reset the algorithm with a custom array when using numbers
  resetCustomNumber(list) {
    for (let index = 0; index < list.length; index++) {
      this.randomArray[index] = parseInt(list[index].value);
    }
    this.randomArrayCopy = this.getCopyOfgeneratedArray();
    this.swaps = 0;
    this.solutionText[0].innerHTML = '<p class = "pass-text"> Pass Number ' + 1 + '</p>';
    this.getSolution();
  }

  // reset the algorithm with a custom array when using strings
  resetCustomString(list) {
    for (let index = 0; index < list.length; index++) {
      this.randomArray[index] = list[index].value;
    }
    this.randomArrayCopy = this.getCopyOfgeneratedArray();
    this.swaps = 0;
    this.solutionText[0].innerHTML = '<p class = "pass-text"> Pass Number ' + 1 + '</p>';
    this.getSolution();
  }


  // generate a random array with n intem (only 6 items for now)
  generateRandomArray(numberItems) {
    let randArray = [];
    if (insertionUsingNumbers) {
      for (var items = 0; items < numberItems; items++) {
        randArray.push(Math.floor(Math.random() * 100));
      }
    } else {
      let alphabet = "abcdefghijklmnopqrstuvwxyz";
      for (var items = 0; items < numberItems; items++) {
        let probability = Math.random();
        if (probability >= 0.5) {
          randArray.push(alphabet.charAt(Math.floor(Math.random() * 26)));
        } else if (probability < 0.5) {
          let tempString = alphabet.charAt(Math.floor(Math.random() * 26));
          tempString += alphabet.charAt(Math.floor(Math.random() * 26));
          randArray.push(tempString);
        }
      }
    }
    return randArray;
  }
  // get a copy of the array, otherwise we work on the solution as arrays are references
  getCopyOfgeneratedArray() {
    return this.randomArray.slice();
  }
  // get the solution of the current array (array sorted and swaps)
  getSolution() {
    for (var index = 1; index < this.randomArray.length; index++) {
      let value = this.randomArray[index];
      let hole = index;
      while (hole > 0 && this.randomArray[hole - 1] > value) {
        this.solutionText[0].innerHTML += '<p class = "swap-text"> Swap [ ' + this.randomArray[hole-1] + ' ] with [ ' + value + ' ]</p>';
        this.randomArray[hole] = this.randomArray[hole - 1];
        hole = hole - 1;
        this.swaps += 1;
      }
      this.randomArray[hole] = value;
    }
    this.solution = this.randomArray;
  }
}