// class bubble sort which performs the algorithm check 
class BubbleSort {

  constructor() {
    this.id = "bubble";
    this.randomArray = this.generateRandomArray(6);
    this.randomArrayCopy = this.getCopyOfgeneratedArray();
    this.hasChanged;
    this.unsortedPart = this.randomArray.length - 1;
    this.passages = 1;
    this.swaps = 0;
    this.solution = [];
    this.solutionText = $('#bubble-solution-body');
    this.solutionText[0].innerHTML = '<p class = "pass-text"> Pass Number ' + this.passages + '</p>';
    this.getSolution();
  }
  // reset the algorithm with a random array
  resetRandom() {
    this.randomArray = this.generateRandomArray(6);
    this.randomArrayCopy = this.getCopyOfgeneratedArray();
    this.unsortedPart = this.randomArray.length - 1;
    this.passages = 1;
    this.solutionText[0].innerHTML = '<p class = "pass-text"> Pass Number ' + this.passages + '</p>';
    this.swaps = 0;
    this.getSolution();
  }
  // reset the algorithm with a custom array when using numbers 
  resetCustomNumber(list) {
    for (let index = 0; index < list.length; index++) {
      this.randomArray[index] = parseInt(list[index].value);
    }
    this.randomArrayCopy = this.getCopyOfgeneratedArray();
    this.unsortedPart = this.randomArray.length - 1;
    this.passages = 1;
    this.solutionText[0].innerHTML = '<p class = "pass-text"> Pass Number ' + this.passages + '</p>';
    this.swaps = 0;
    this.getSolution();
  }

  // reset the algorithm with a custom array when using strings 
  resetCustomString(list) {
    for (let index = 0; index < list.length; index++) {
      this.randomArray[index] = list[index].value;
    }
    this.randomArrayCopy = this.getCopyOfgeneratedArray();
    this.unsortedPart = this.randomArray.length - 1;
    this.passages = 1;
    this.solutionText[0].innerHTML = '<p class = "pass-text"> Pass Number ' + this.passages + '</p>';
    this.swaps = 0;
    this.getSolution();
  }


  // generate a random array with n intem (only 6 items for now)
  generateRandomArray(numberItems) {
    let randArray = [];
    if (bubbleUsingNumbers) {
      for (var items = 0; items < numberItems; items++) {
        randArray.push(Math.floor(Math.random() * 100));
      }
    } else {
      let alphabet = "abcdefghijklmnopqrstuvwxyz";
      for (var items = 0; items < numberItems; items++) {
        let probability = Math.random();
        console.log(probability + "Prob");
        if (probability >= 0.5) {
          randArray.push(alphabet.charAt(Math.floor(Math.random() * 26)));
        } else if (probability < 0.5) {
          let tempString = alphabet.charAt(Math.floor(Math.random() * 26));
          tempString += alphabet.charAt(Math.floor(Math.random() * 26));
          randArray.push(tempString);
        }
      }
    }
    return randArray;
  }
  // get a copy of the array, otherwise we work on the solution as arrays are references
  getCopyOfgeneratedArray() {
    return this.randomArray.slice();
  }
  // get the solution of the current array (array sorted, passes and swaps)
  getSolution() {
    let lastSwap = false;
    do {
      this.hasChanged = false;
      for (var index = 0; index < this.unsortedPart; index++) {
        if (this.randomArray[index] > this.randomArray[index + 1]) {
          this.solutionText[0].innerHTML += '<p class = "swap-text"> Swap [ ' + this.randomArray[index] + ' ] with [ ' + this.randomArray[index +1] + ' ]</p>';
          var temp = this.randomArray[index];
          this.randomArray[index] = this.randomArray[index + 1];
          this.randomArray[index + 1] = temp;
          this.hasChanged = true;
          this.swaps += 1;
        }
        if (this.unsortedPart == 1 && this.hasChanged) {
          this.hasChanged = false;
          lastSwap = true;
        }
      }
      this.unsortedPart = this.unsortedPart - 1;
      if (this.hasChanged) {
        this.passages += 1;
        this.solutionText[0].innerHTML += '<p class = "pass-text"> Pass Number ' + this.passages + '</p>';
      } else if (lastSwap) {
        this.solutionText[0].innerHTML += '<p class = "swap-text"></p>';
      } else {
        this.solutionText[0].innerHTML += '<p class = "swap-text">No Swap</p>';
      }

    }
    while (this.hasChanged);
    this.solution = this.randomArray;
    console.log(this.passages);
  }
}