var mainAimationFinished = false;
var showTutorial = false;

$(document).ready(function () {
  var $introContainer = $('#introContainer');
  var $beginButton = $('#begin');
  var $introImageActive = $('#introImageActive');

  setTimeout(function () {
    $introContainer.fadeIn(4000);
  }, 1000);

  $beginButton.on('click', function () {
    $introImageActive.fadeIn(2000, function () {
      $introContainer.fadeOut(2000, function () {
        mainAimationFinished = true;
      });
    });
  });

});