// Bubble Sort Algorithms
function bubble_Sort(array) {
  var hasChanged;
  var passages = 0;
  var unsortedPart = array.length - 1;
  do {
    hasChanged = false;
    for (var index = 0; index < array.length - 1; index++) {
      if (array[index] > array[index + 1]) {
        var temp = array[index];
        array[index] = array[index + 1];
        array[index + 1] = temp;
        hasChanged = true;
      }
    }
    unsortedPart = unsortedPart - 1;
    passages++;
  }
  while (hasChanged);
  var solution = [array, passages];
  return solution;
}

! function () {
  let myArray = [5, 1, 4, 2, 8, 9];
  let originalArray = myArray.slice();
  let solution = bubble_Sort(myArray);
  console.log("myArray: " + originalArray + " becomes: " + solution[0]);
  console.log("Number of passages: " + solution[1]);
}();

// Insertion Sort Algorithms

function insertion_Sort(array) {
  var value;
  var hole;
  var passages = 1;

  for (var index = 1; index < array.length; index++) {
    value = array[index];
    hole = index;

    while (hole > 0 && array[hole - 1] > value) {
      array[hole] = array[hole - 1];
      hole = hole - 1;
    }
    array[hole] = value;

  }

  var solution = [array, passages];
  return solution;
}

! function () {
  let myArray = [5, 1, 4, 2, 8, 9];
  let originalArray = myArray.slice();
  let solution = insertion_Sort(myArray);
  console.log("myArray: " + originalArray + " becomes: " + solution[0]);
  console.log("Number of passages: " + solution[1]);
}();

// Quick Sort Algorithms

function quick_Sort(array) {
  if (array.length <= 1) return array;

  var pivot = array[array.length - 1];
  var leftPartition = [];
  var rightPartition = [];

  for (var index = 0; index < array.length - 1; index++) {
    if (array[index] < pivot) {
      leftPartition.push(array[index]);
    } else rightPartition.push(array[index]);

  }

  return [...quick_Sort(leftPartition), pivot, ...quick_Sort(rightPartition)];
}

! function () {
  let myArray = [5, 1, 4, 2, 8, 9];
  let originalArray = myArray.slice();
  let solution = quick_Sort(myArray);
  console.log("myArray: " + originalArray + " becomes: " + solution);
}();

// Quick Sort Algorithms

function merge_Sort(array) {
  if (array.length === 1) return array;

  var middle = Math.floor(array.length / 2);
  var leftPartition = array.slice(0, middle);
  var rightPartition = array.slice(middle);

  return merge(merge_Sort(leftPartition), merge_Sort(rightPartition));
}

function merge(leftPartition, rightPartition) {
  var result = []
  var indexLeft = 0
  var indexRight = 0

  while (indexLeft < leftPartition.length && indexRight < rightPartition.length) {
    if (leftPartition[indexLeft] < rightPartition[indexRight]) {
      result.push(leftPartition[indexLeft])
      indexLeft++
    } else {
      result.push(rightPartition[indexRight])
      indexRight++
    }
  }

  return result.concat(leftPartition.slice(indexLeft)).concat(rightPartition.slice(indexRight))
}

! function () {
  let myArray = [5, 1, 4, 2, 8, 9];
  let originalArray = myArray.slice(0);
  let solution = merge_Sort(myArray);
  console.log("myArray: " + originalArray + " becomes: " + solution);
}();